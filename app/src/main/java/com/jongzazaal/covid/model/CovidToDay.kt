package com.jongzazaal.covid.model

import com.google.gson.annotations.SerializedName

data class CovidToDay(
    @SerializedName("total_case") val confirm: Int,
    @SerializedName("total_recovered") val recovered: Int,
    @SerializedName("Hospitalized") val hospitalized: Int,
    @SerializedName("total_death") val deaths: Int,
    @SerializedName("new_case") val newConfirmed: Int,
    @SerializedName("new_recovered") val newRecovered: Int,
    @SerializedName("NewHospitalized") val newHospitalized: Int,
    @SerializedName("new_death") val newDeaths: Int,
    @SerializedName("update_date") val UpdateDate: String
)