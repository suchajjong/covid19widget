package com.jongzazaal.covid.widget

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.RemoteViews
import com.jongzazaal.covid.R
import com.jongzazaal.covid.model.CovidToDay
import com.jongzazaal.covid.network.CovidAPI
import com.jongzazaal.covid.utility.toNumberComma
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class MyWidgetProvider : AppWidgetProvider() {
    override fun onReceive(context: Context?, intent: Intent?) {
        super.onReceive(context, intent)
//        val s = intent?.getStringExtra("test_id")
//        val listS = intent?.getStringArrayExtra("test_id")
//        val listS2 = intent?.getStringArrayListExtra("test_id")

//        intent?.getStringExtra("test_id")
        val extras = intent?.getExtras()
        if (extras != null) {
            val appWidgetManager = AppWidgetManager.getInstance(context)
            val thisAppWidget = ComponentName(
                context!!.getPackageName(),
                MyWidgetProvider::class.java.getName()
            )
            val appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidget)

            onUpdate(context, appWidgetManager, appWidgetIds)
        }
    }

    override fun onUpdate(
        context: Context?,
        appWidgetManager: AppWidgetManager?,
        appWidgetIds: IntArray?
    ) {
        appWidgetIds?.forEach {appWidgetId ->
            //            val number = String.format("%03d", Random().nextInt(900) + 100)
//
//            val pendingIntent: PendingIntent = Intent(context, MyWidgetProvider::class.java)
//                .let { intent ->
//                    intent.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
//                    intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)
//                    intent.putExtra("test_id", number)
//                    PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
//                }
//
//            val pendingIntentOpenDetail: PendingIntent = Intent(context, DetailActivity::class.java)
//                .let { intent ->
//                    intent.putExtra(DetailActivity.PAGE_ID, number)
//                    intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)
//                    PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
//                }
//
//            val views: RemoteViews = RemoteViews(
//                context?.packageName,
//                R.layout.widget_a
//            ).apply {
//                setOnClickPendingIntent(R.id.actionButton, pendingIntent)
//                setOnClickPendingIntent(R.id.button, pendingIntentOpenDetail)
//                setTextViewText(R.id.textView, number)
//            }
//
//            appWidgetManager?.updateAppWidget(appWidgetId, views)
            val remoteViews = RemoteViews(
                context?.packageName,
                R.layout.widget_covid_all_i
            )
            val pendingIntent: PendingIntent = Intent(context, MyWidgetProvider::class.java)
                .let { intent ->
                    intent.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
                    intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds)
                    PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
                }
            remoteViews.setOnClickPendingIntent(R.id.button, pendingIntent)
            callNetwork(remoteViews, appWidgetManager, appWidgetId, context)
            appWidgetManager?.updateAppWidget(appWidgetId, remoteViews)

        }
    }
    private fun callNetwork(remoteViews: RemoteViews, appWidgetManager: AppWidgetManager?, appWidgetId: Int, context: Context?){
        setViewAll(remoteViews, appWidgetManager, appWidgetId,context?.getString(R.string.loading)?:"")
        var retrofit: Retrofit = Retrofit.Builder()
            .baseUrl("https://covid19.ddc.moph.go.th")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
        retrofit.create<CovidAPI>(CovidAPI::class.java)
            .getToDay()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<List<CovidToDay>> {
                override fun onComplete() {

                }

                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(model: List<CovidToDay>) {
                    val t = model.first()
                    remoteViews.setTextViewText(R.id.textView_UpdateDate, context?.getString(R.string.UpdateDate)?.format(t.UpdateDate))
                    remoteViews.setTextViewText(R.id.textView_confirmed, t.confirm.toNumberComma())
                    remoteViews.setTextViewText(R.id.textView_newConfirmed, context?.getString(R.string.updatePlus)?.format(t.newConfirmed.toNumberComma()))
                    remoteViews.setTextViewText(R.id.textView_recovered, t.recovered.toNumberComma())
                    remoteViews.setTextViewText(R.id.textView_newRecovered, context?.getString(R.string.updatePlus)?.format(t.newRecovered.toNumberComma()))
                    remoteViews.setTextViewText(R.id.textView_hospitalized, t.hospitalized.toNumberComma())
                    remoteViews.setTextViewText(R.id.textView_newHospitalized, context?.getString(R.string.update)?.format(t.newHospitalized.toNumberComma()))
                    remoteViews.setTextViewText(R.id.textView_deaths, t.deaths.toNumberComma())
                    remoteViews.setTextViewText(R.id.textView_newDeaths, context?.getString(R.string.updatePlus)?.format(t.newDeaths.toNumberComma()))


//                    remoteViews.setTextViewText(R.id.textView_recovered, context?.getString(R.string.recovered)?.format(t.recovered))
//                    remoteViews.setTextViewText(R.id.textView_hospitalized, context?.getString(R.string.hospitalized)?.format(t.hospitalized))
//                    remoteViews.setTextViewText(R.id.textView_deaths, context?.getString(R.string.deaths)?.format(t.deaths))
//                    remoteViews.setTextViewText(R.id.textView_newConfirmed, context?.getString(R.string.newConfirmed)?.format(t.newConfirmed))
//                    remoteViews.setTextViewText(R.id.textView_newRecovered, context?.getString(R.string.newRecovered)?.format(t.newRecovered))
//                    remoteViews.setTextViewText(R.id.textView_newHospitalized, context?.getString(R.string.newHospitalized)?.format(t.newHospitalized))
//                    remoteViews.setTextViewText(R.id.textView_newDeaths, context?.getString(R.string.newDeaths)?.format(t.newDeaths))
//                    remoteViews.setTextViewText(R.id.textView_UpdateDate, context?.getString(R.string.UpdateDate)?.format(t.UpdateDate))

                    appWidgetManager?.updateAppWidget(appWidgetId, remoteViews)
                }

                override fun onError(e: Throwable) {
                    setViewAll(remoteViews, appWidgetManager, appWidgetId,context?.getString(R.string.error)?:"")
                    appWidgetManager?.updateAppWidget(appWidgetId, remoteViews)
                }

            })

    }
    private fun setViewAll(remoteViews: RemoteViews, appWidgetManager: AppWidgetManager?, appWidgetId: Int, text:String){
//        remoteViews.setTextViewText(R.id.textView_confirmed, text)
//        remoteViews.setTextViewText(R.id.textView_recovered, text)
//        remoteViews.setTextViewText(R.id.textView_hospitalized, text)
//        remoteViews.setTextViewText(R.id.textView_deaths, text)
//        remoteViews.setTextViewText(R.id.textView_newConfirmed, text)
//        remoteViews.setTextViewText(R.id.textView_newRecovered, text)
//        remoteViews.setTextViewText(R.id.textView_newHospitalized, text)
//        remoteViews.setTextViewText(R.id.textView_newDeaths, text)
        remoteViews.setTextViewText(R.id.textView_UpdateDate, text)
        appWidgetManager?.updateAppWidget(appWidgetId, remoteViews)
    }

    override fun onAppWidgetOptionsChanged(
        context: Context?,
        appWidgetManager: AppWidgetManager?,
        appWidgetId: Int,
        newOptions: Bundle?
    ) {
        super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId, newOptions)
    }

    override fun onRestored(context: Context?, oldWidgetIds: IntArray?, newWidgetIds: IntArray?) {
        super.onRestored(context, oldWidgetIds, newWidgetIds)
    }

    override fun onEnabled(context: Context?) {
        super.onEnabled(context)
    }

    override fun onDisabled(context: Context?) {
        super.onDisabled(context)
    }

}