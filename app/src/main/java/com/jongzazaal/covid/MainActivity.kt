package com.jongzazaal.covid

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.jongzazaal.covid.model.CovidToDay
import com.jongzazaal.covid.network.CovidAPI
import com.jongzazaal.covid.utility.toNumberComma
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.widget_covid_all_i.*
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.widget_covid_all_i)
        callNetwork()
        button.setOnClickListener {
            callNetwork()
        }
    }

    private fun setViewAll(text: String) {
        textView_UpdateDate.text = text
    }

    private fun callNetwork() {
        setViewAll(getString(R.string.loading))
        var retrofit: Retrofit = Retrofit.Builder()
            .baseUrl("https://covid19.ddc.moph.go.th")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
        retrofit.create<CovidAPI>(CovidAPI::class.java)
            .getToDay()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<List<CovidToDay>> {
                override fun onComplete() {

                }

                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(model: List<CovidToDay>) {
                    val t = model.first()
                    textView_UpdateDate.text = getString(R.string.UpdateDate).format(t.UpdateDate)
                    textView_confirmed.text = t.confirm.toNumberComma()
                    textView_newConfirmed.text = getString(R.string.updatePlus).format(t.newConfirmed.toNumberComma())
                    textView_recovered.text = t.recovered.toNumberComma()
                    textView_newRecovered.text = getString(R.string.updatePlus).format(t.newRecovered.toNumberComma())
                    textView_hospitalized.text = t.hospitalized.toNumberComma()
                    textView_newHospitalized.text = getString(R.string.update).format(t.newHospitalized.toNumberComma())
                    textView_deaths.text = t.deaths.toNumberComma()
                    textView_newDeaths.text = getString(R.string.updatePlus).format(t.newDeaths.toNumberComma())

                }

                override fun onError(e: Throwable) {
                    setViewAll(getString(R.string.error))
                }

            })

    }
}
