package com.jongzazaal.covid.network

import com.jongzazaal.covid.model.CovidToDay
import io.reactivex.Observable
import retrofit2.http.GET

interface CovidAPI {
    @GET("/api/Cases/today-cases-all")
    fun getToDay(): Observable<List<CovidToDay>>
}