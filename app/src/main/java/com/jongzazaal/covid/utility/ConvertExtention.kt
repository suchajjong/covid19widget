package com.jongzazaal.covid.utility

import java.text.DecimalFormat

fun String?.toNumberComma(): String {
    if (this.isNullOrEmpty()){
        return "0"
    }
    val str = this.replace(",", "")
    val formatter = DecimalFormat("###,###,###,###")
    formatter.negativePrefix = "- "
    val yourFormattedString = formatter.format(str.toFloat())
    return yourFormattedString
}

fun Int?.toNumberComma(): String {
    if (this.toString().isEmpty()){
        return "0"
    }
    val str = this.toString().replace(",", "")
    val formatter = DecimalFormat("###,###,###,###")
    formatter.negativePrefix = "- "
    val yourFormattedString = formatter.format(str.toFloat())
    return yourFormattedString
}